package edu.geometry;

import java.util.Arrays;

public class Polygon extends GeomFigure implements WithAngles{
    protected final Point[] points;

    Polygon(Point... points) {
        if (points.length < 3) { throw new IllegalArgumentException("Полигон должен иметь хотя бы 3 точки!"); }
        this.points = points;
    }
    @Override
    public void printAllVertexes() {
        System.out.println(Arrays.toString(points));
    }

    @Override
    public double getArea() {
        // используем форму Гаусса
        double productAll = 0;
        for (int i = 1; i < points.length; i++) {
            productAll += points[i-1].getX() * points[i].getY() - points[i-1].getY() * points[i].getX();
        }
        int N = points.length;
        productAll += points[N-1].getX() * points[0].getY() - points[N-1].getY() * points[0].getX();

        return Math.abs(productAll) / 2;
    }

    @Override
    public double getPerimeter() {
        double perimeter = 0;
        for (int i = 1; i < points.length; i++) {
            perimeter += Math.hypot(points[i-1].getX() - points[i].getX(), points[i-1].getY() - points[i].getY());
        }
        int N = points.length;
        // посчиать первую и полсденюю точки
        perimeter += Math.hypot(points[0].getX() - points[N-1].getX(), points[0].getY() - points[N-1].getY());
        return perimeter;
    }
}
