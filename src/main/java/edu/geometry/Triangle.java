package edu.geometry;

public class Triangle extends GeomFigure{
    private final Point firstVertex;
    private final Point secondVertex;
    private final Point thirdVertex;

    @Override
    public double getArea() {
        // использую векторное произведение
        Point firstVector = firstVertex.makeVector(secondVertex);
        Point secondVector = firstVertex.makeVector(thirdVertex);
        // значение -- площадь параллелограмма
        double vectorAbsoluterProduct = firstVector.getX() * secondVector.getY() -
                firstVector.getY() * secondVector.getX();
        return Math.abs(vectorAbsoluterProduct) / 2;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "firstVertex=" + firstVertex +
                ", secondVertex=" + secondVertex +
                ", thirdVertex=" + thirdVertex +
                '}';
    }

    public Triangle(Point firstVertex, Point secondVertex, Point thirdVertex) {
        this.firstVertex = firstVertex;
        this.secondVertex = secondVertex;
        this.thirdVertex = thirdVertex;
    }

    @Override
    public double getPerimeter() {
        return Math.hypot(firstVertex.getX() - secondVertex.getX(), firstVertex.getY() - secondVertex.getY()) +
                Math.hypot(firstVertex.getX() - thirdVertex.getX(), firstVertex.getY() - thirdVertex.getY() +
                Math.hypot(secondVertex.getX() - thirdVertex.getX(), secondVertex.getY() - thirdVertex.getY()));
    }

    // Возращает true если тупоугольный,
    // в противном случае -- false
    public boolean isObtuse() {
        // проверяем каждый угол
        Point firstVector = firstVertex.makeVector(secondVertex);
        Point secondVector = firstVertex.makeVector(thirdVertex);
        double scalarProduct = firstVector.getX() * secondVector.getX() + firstVector.getY() * secondVector.getY();
        if (scalarProduct < 0) { return true; }

        firstVector = secondVertex.makeVector(firstVertex);
        secondVector = secondVertex.makeVector(thirdVertex);
        scalarProduct = firstVector.getX() * secondVector.getX() + firstVector.getY() * secondVector.getY();
        if (scalarProduct < 0) { return true; }

        firstVector = thirdVertex.makeVector(secondVertex);
        secondVector = thirdVertex.makeVector(firstVertex);
        scalarProduct = firstVector.getX() * secondVector.getX() + firstVector.getY() * secondVector.getY();
        return scalarProduct < 0;
    }

}
