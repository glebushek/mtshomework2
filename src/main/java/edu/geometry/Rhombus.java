package edu.geometry;

public class Rhombus extends Polygon implements EqualSizes{
    private final double size;

    Rhombus(double size, Point... points) {
        super(points);
        this.size = size;
    }


    @Override
    public void printSize() {
        System.out.println(size);
    }

    public void printRhombusInfo() {
        System.out.println(
                "This is Area of rhombus: " + getArea() + "\n" +
                "This is perimeter of rhombus: " + getPerimeter()
        );
    }
}
