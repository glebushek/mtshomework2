package edu.geometry;

public abstract class GeomFigure {
    public abstract double getArea();

    public abstract double getPerimeter();
}
