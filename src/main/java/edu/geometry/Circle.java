package edu.geometry;

public class Circle extends GeomFigure{
    private final double radius;
    private final Point center;

    private String color;

    public Circle(double radius, double center_x, double center_y, String color) {
        if (radius < 0) { throw new IllegalArgumentException("Радиус должен быть неотрицательным!"); }
        this.radius = radius;
        this.center = new Point(center_x, center_y);
        this.color = color;
    }

    @Override
    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    public void colorCircle(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", center=" + center +
                ", color='" + color + '\'' +
                '}';
    }
}
