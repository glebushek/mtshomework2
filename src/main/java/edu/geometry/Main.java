package edu.geometry;

public class Main {
    public static void main(String[] args) {
        // 1. Тестим Круг
        Circle circle = new Circle(1, 1, -1, "YELLOW");
        System.out.println(circle);
        System.out.println("Circle's Area: " + circle.getArea());
        System.out.println("Circle's Area: " + circle.getPerimeter());
        circle.colorCircle("BLACK");
        System.out.println("Changed color to BLACK");
        System.out.println(circle);
        System.out.println("----------------------------------------------------------------------------");

        // 2. Тестим Triangle
        Point a = new Point(0, 3), b = new Point(4, 0), c = new Point(0, 0);
        Triangle triangle = new Triangle(a, b, c);
        System.out.println(triangle);
        System.out.println("Triangle's perimeter: " + triangle.getPerimeter() + ", should be 12");
        System.out.println("Triangle's area: " + triangle.getArea() + ", should be 6");
        System.out.println("----------------------------------------------------------------------------");

        // 3. Тестим Polygon
        Polygon polygon = new Polygon(a, b, c);
        polygon.printAllVertexes();
        System.out.println("Polygon's perimeter: " + polygon.getPerimeter() + ", should be 12");
        System.out.println("Polygon's area: " + polygon.getArea() + ", should be 6");
        System.out.println("----------------------------------------------------------------------------");

        // 4. Тестим ромб
         a = new Point(-1, 0);
         c = new Point(1, 0);
         b = new Point(0, 1);
         Point d = new Point(0, -1);
        Rhombus rhombus = new Rhombus(1, a, b, c, d);
        rhombus.printRhombusInfo();
        rhombus.printSize();

        System.out.println("----------------------------------------------------------------------------");
        // 5. Тестим тупоугольный треугольник
        a = new Point(-1000, 0);
        b = new Point(1000, 0);
        c = new Point(0, 1);
        triangle = new Triangle(a, b, c);
        System.out.println("Тупоугольный? " + triangle.isObtuse() + ". Should be true");


    }
}
